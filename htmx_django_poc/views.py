from django.shortcuts import render


def index(request):
    return render(request, "index.html")


def hello(request):
    return render(request, "hello.html", context={"name": request.GET.get("name")})
